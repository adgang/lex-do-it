enum Occupation {
  TaxAccountant,
  TaxManager,
}

interface OrgProfile {
  location: String;
  occupation: Occupation;
}

interface FinancialStatement {
  profit?: Number;
  assets: Number;
  liabilities: Number;
  equity: Number;
  taxLiability: Number;
  taxPaid: Number;
  interestAccrued: Number;
  interestPaid: Number;
  depreciation: Number;
}

interface TaxLiability {
  name: String;
  value: Number;
}

interface SaleTransaction {
  id: String; // Transaction Id
  value: Number; // Example: 10,000 PLN
  productOrServiceCode: String; // Example: PKD code such as 88.33.Z
  taxLiabilities: Map<string, TaxLiability>;
  // Tax Regulation to Tax mapping:
  // {
  //   2020_Act_12_Section_221: {
  //      name: "VAT",
  //      value: 123.43
  //   }
  //   2020_Act_11_Section_12: {
  //      name: "Municipal Tax",
  //      value: 12.34
  //   }
  // }
}

export interface OrgState {
  lastStatement?: FinancialStatement;
  orgProfile?: OrgProfile;
  preferences?: any;
}

export class IndirectTaxRule {
  constructor(
    origin: String,
    name: String,
    calculation: (state: OrgState, sale: SaleTransaction) => any
  ) {
    this.origin = origin;
    this.name = name;
    this.calculation = calculation;
  }
  origin: String; // Article/Section of Act imposing the tax
  name: String; // name of tax imposed
  calculation: (state: OrgState, sale: SaleTransaction) => any;
}

export class DirectTaxRule {
  constructor(
    origin: String,
    name: String,
    calculation: (state: OrgState) => any
  ) {
    this.origin = origin;
    this.name = name;
    this.calculation = calculation;
  }
  origin: String; // Article/Section of Act imposing the tax
  name: String; // name of tax imposed
  calculation: (state: OrgState) => any;
}
