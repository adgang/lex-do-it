import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useRef } from "react";
import Paper from "@material-ui/core/Paper";
import styles from "../styles/Vat.module.css";
import { flattenDiagnosticMessageText } from "typescript";

function CompanyDetails(props: any): any {
  const companyIdRef: any = useRef();
  const companyTurnoverRef: any = useRef();
  return (
    <div>
      <Paper className={styles.surface}>
        <div className={styles.grid}>
          <div className={styles.gridRow}>
            <label className={styles.gridItem}>Company Registration ID:</label>
            <input
              className={styles.gridItem}
              type="text"
              ref={companyIdRef.current}
            ></input>
          </div>
          <div className={styles.gridRow}>
            <label className={styles.gridItem}> Company Turnover:</label>
            <input
              className={styles.gridItem}
              type="text"
              ref={companyTurnoverRef.current}
            ></input>
          </div>
        </div>
      </Paper>
    </div>
  );
}

function BusinessOfferings(props: any): any {
  const companyIdRef: any = useRef();
  const companyTurnoverRef: any = useRef();
  return (
    <div>
      <Paper className={styles.surface}>
        <div className={styles.gridRow}>
          <label className={styles.gridItem}>
            PKD IDs(based on Registration ID):
          </label>
          {/* TODO: call API to get this values */}
          <label className={styles.gridItem}>88.06.Z, 86.07.Z</label>
        </div>
        <div className={styles.grid}>
          <div className={styles.gridRow}>
            <label className={styles.gridItem}>Services:</label>
            <select className={styles.gridItem} ref={companyIdRef.current}>
              <option value="Tax Accountancy">Tax Accountancy</option>
              <option value="Tax Management">Tax Management</option>
            </select>
          </div>
        </div>
      </Paper>
    </div>
  );
}

function TaxImplications(props: any) {
  return (
    <div>
      <Paper className={styles.surface}>
        <div className={styles.gridRow}>
          <label className={styles.gridItem}>
            <p>
              <b>VAT Registration:</b>
            </p>
          </label>
          {/* TODO: call API to get this values */}
          <label className={styles.gridItem}>
            You have to get a mandatory VAT registration.
          </label>
        </div>
        <div className={styles.grid}>
          <div className={styles.gridRow}>
            <p>
              <b>VAT Rate:</b>
            </p>
            <label className={styles.gridItem}>
              A VAT rate of 10% is applicable on your services.
            </label>
          </div>
        </div>
      </Paper>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    "Enter Company Details",
    "Enter Business Offerings",
    "Tax Implications",
  ];
}

function getStepContent(step: number) {
  switch (step) {
    case 0:
      return <CompanyDetails></CompanyDetails>;
    case 1:
      return <BusinessOfferings />;
    case 2:
      return <TaxImplications />;
    default:
      return "Unknown step";
  }
}

export default function TaxStepper() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const steps = getSteps();

  const isStepOptional = (step: number) => {
    return false;
    // return step === 1;
  };

  const isStepSkipped = (step: number) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  interface StepProps {
    completed: boolean;
  }

  interface LabelProps {
    optional: any;
  }

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps: StepProps = { completed: false };
          const labelProps: LabelProps = { optional: false };
          if (isStepOptional(index)) {
            labelProps.optional = (
              <Typography variant="caption">Optional</Typography>
            );
          }
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Back
              </Button>
              {isStepOptional(activeStep) && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleSkip}
                  className={classes.button}
                >
                  Skip
                </Button>
              )}

              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? "Finish" : "Next"}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
