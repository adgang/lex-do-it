import React from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import Link from "next/link";
import gitlabLogo from "../public/gitlab-icon-rgb.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckSquare } from "@fortawesome/free-solid-svg-icons";
import TaxForm from "../components/TaxForm";

export default function Vat() {
  return (
    <div className={styles.container}>
      <Head>
        <title>LexDoIt</title>
        <meta name="description" content="Simplifying Compliance" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <header>
          <h1 className={styles.title}>
            LexDoIt
            <sup className={styles.sup}>
              <FontAwesomeIcon icon={faCheckSquare}></FontAwesomeIcon>
            </sup>
          </h1>
          <p className={styles.description}>
            <code className={styles.code}>Simpliflying Compliance</code>
          </p>
        </header>
        <TaxForm></TaxForm>
      </main>

      <footer className={styles.footer}>
        <Link href="/">Home</Link>
        <a
          href="https://gitlab.com/adgang/lex-do-it"
          target="_blank"
          rel="noopener noreferrer"
        >
          Code{" "}
          <Image src={gitlabLogo} alt="Gitlab Logo" width={72} height={16} />
        </a>
        <Link href="/about">About</Link>
      </footer>
    </div>
  );
}
